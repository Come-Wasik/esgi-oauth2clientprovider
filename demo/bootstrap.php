<?php

session_start();

function base_path(string $path = '')
{
    return dirname(__DIR__) . (!empty($path) ? '/' . $path : '');
}

function view_path(string $path = '')
{
    return base_path('demo/views') . (!empty($path) ? '/' . $path : '');
}

require base_path('demo/controllers/MainController.php');
require base_path('demo/controllers/AuthorizationCodeController.php');
require base_path('demo/controllers/PasswordController.php');
require base_path('demo/controllers/ClientCredentialController.php');

define('WEBSITE_URL', 'http://localhost:8080');
