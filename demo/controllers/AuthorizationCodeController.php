<?php

use GroupArbCw\Oauth2\Client\Provider\Battlenet;
use GroupArbCw\Oauth2\Client\Provider\Discord;

class AuthorizationCodeController
{
    public function authDiscord()
    {
        $authorizeUrl = 'https://discord.com/api/oauth2/authorize';

        $params = [
            'response_type' => 'code',
            'client_id' => '732254875986952212',
            'redirect_uri' => 'http://localhost:8080/auth-code/data-discord',
            'scope' => 'identify',
            'state' => 'xyz'
        ];
        $autorizeFullUrl = $authorizeUrl . '?' . http_build_query($params);

        header('Location: ' . $autorizeFullUrl);
        die();
    }

    public function dataDiscord()
    {
        # Récupération du token 
        var_dump($_GET);

        $tokenUrl = 'https://discord.com/api/oauth2/token';
        $params = [
            'client_id' => '732254875986952212',
            'client_secret' => '-nwpjGNnGCVW9svGtc_67vWaJGlZOY1d',
            'grant_type' => 'authorization_code',
            'code' => $_GET['code'],
            'redirect_uri' => 'http://localhost:8080/auth-code/data-discord',
            'scope' => 'identify'
        ];

        $context = stream_context_create([
            'http' => [
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($params),
            ]
        ]);

        var_dump($context);
        $response = file_get_contents($tokenUrl, false, $context);
        $content = json_decode($response, true);
        var_dump($content);

        # Utilisation du token
        $meUrl = 'https://discordapp.com/api/users/@me';
        $context = stream_context_create([
            'http' => [
                'header' => 'authorization: ' . $content['token_type'] . ' ' . $content['access_token'] . "\r\n",
                'method' => 'GET',
            ]
        ]);
        $response = file_get_contents($meUrl, false, $context);
        $content = json_decode($response, true);
        var_dump($content);
    }

    public function authBattlenet()
    {
        $authorizeUrl = 'https://eu.battle.net/oauth/authorize';

        $params = [
            'response_type' => 'code',
            'client_id' => '5760ca88a6ef4270a7b29c5bea776f85',
            'redirect_uri' => 'http://localhost:8080/auth-code/data-battlenet',
            'scope' => 'wow.profile',
            'state' => 'xyz'
        ];
        $autorizeFullUrl = $authorizeUrl . '?' . http_build_query($params);

        header('Location: ' . $autorizeFullUrl);
        die();
    }

    public function dataBattlenet()
    {
        # Récupération du token
        var_dump($_GET);

        $tokenUrl = 'https://eu.battle.net/oauth/token';
        $params = [
            'client_id' => '5760ca88a6ef4270a7b29c5bea776f85',
            'client_secret' => 'OH6Hi1aQSAGTePcDJCdFvuLY9SWpnvqj',
            'grant_type' => 'authorization_code',
            'code' => $_GET['code'],
            'redirect_uri' => 'http://localhost:8080/auth-code/data-battlenet',
            'scope' => 'wow.profile'
        ];

        $context = stream_context_create([
            'http' => [
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($params),
            ]
        ]);

        var_dump($context);
        $response = file_get_contents($tokenUrl, false, $context);
        $content = json_decode($response, true);
        var_dump($content);

        # Utilisation du token
        $meUrl = 'https://eu.battle.net/oauth/userinfo';
        $context = stream_context_create([
            'http' => [
                'header' => 'authorization: ' . $content['token_type'] . ' ' . $content['access_token'] . "\r\n",
                'method' => 'GET',
            ]
        ]);
        $response = file_get_contents($meUrl, false, $context);
        $content = json_decode($response, true);
        var_dump($content);
    }

    public function discordProvider()
    {
        $provider = new Discord([
            'clientId' => '732254875986952212',
            'clientSecret' => '-nwpjGNnGCVW9svGtc_67vWaJGlZOY1d',
            'redirectionUrl' => 'http://localhost:8080/provider-discord/auth-code',
            'scope' => 'identify',
        ]);
        $provider->useAuthorizationCode();
        var_dump($provider);

        if (!isset($_GET['code'])) {
            # Ask code
            $_SESSION['oauth2_state'] = $provider->getState();
            $provider->sendAuthorizeRequest();
        } else if (empty($_GET['state']) || $_SESSION['oauth2_state'] !== $_GET['state']) {
            # Error
            unset($_SESSION['oauth2_state']);
            die('Authentification error');
        } else {
            # Get token with code and show data
            $token = $provider->getToken([
                'code' => $_GET['code']
            ]);
            var_dump($token);

            $user = $provider->getResourceOwner($token);
            var_dump($user);
        }
    }

    public function battlenetProvider()
    {
        $provider = new Battlenet([
            'clientId' => '5760ca88a6ef4270a7b29c5bea776f85',
            'clientSecret' => 'OH6Hi1aQSAGTePcDJCdFvuLY9SWpnvqj',
            'redirectionUrl' => 'http://localhost:8080/provider-battlenet/auth-code',
            'scope' => 'wow.profile',
        ]);
        $provider->useAuthorizationCode();
        var_dump($provider);

        if (!isset($_GET['code'])) {
            # Ask code
            $_SESSION['oauth2_state'] = $provider->getState();
            $provider->sendAuthorizeRequest();
        } else if (empty($_GET['state']) || $_SESSION['oauth2_state'] !== $_GET['state']) {
            # Error
            unset($_SESSION['oauth2_state']);
            die('Authentification error');
        } else {
            # Get token with code and show data
            $token = $provider->getToken([
                'code' => $_GET['code']
            ]);
            var_dump($token);

            $user = $provider->getResourceOwner($token);
            var_dump($user);
        }
    }
}
