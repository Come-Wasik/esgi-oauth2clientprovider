<?php

class MainController
{
    public function home()
    {
        require view_path('home.php');
    }

    public function logout()
    {
        session_destroy();
        header('Location: ' . WEBSITE_URL);
        die();
    }
}
