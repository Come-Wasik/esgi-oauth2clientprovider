<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Accueil</title>
</head>

<body>
    <ul>
        <h3 style="margin: 0.3em;">Auth code "A la main"</h3>
        <li><a href="/auth-code/auth-discord">Start auth for discord</a></li>
        <li><a href="/auth-code/auth-battlenet">Start auth for Battlenet</a></li>
        <li><a href="/auth-code/data-discord">discord result</a></li>
        <li><a href="/auth-code/data-battlenet">Battlenet result</a></li>
        <br>
        <h3 style="margin: 0.3em;">Auth code by using providers</h3>
        <li><a href="/provider-discord/auth-code">Use Discord provider</a></li>
        <li><a href="/provider-battlenet/auth-code">Use Battlenet provider</a></li>
        <br>
        <h3 style="margin: 0.3em;">Client credentials by using provider</h3>
        <li><a href="/provider-discord/client-credential">Use Discord provider</a></li>
        <li><a href="/provider-battlenet/client-credential">Use Battlenet provider</a></li>
        <br>
        <h3 style="margin: 0.3em;">Other</h3>
        <li><a href="/logout">Logout</a></li>
    </ul>
</body>

</html>