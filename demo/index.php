<?php

require __DIR__ . '/bootstrap.php';
require dirname(__DIR__) . '/autoload.php';

$routes = require base_path('demo/routes.php');

$uri = strtok($_SERVER['REQUEST_URI'], '?');

if (key_exists($uri, $routes)) {
    list($controller, $method) = explode('@', $routes[$uri]);

    (new $controller)->$method();
} else {
    (new MainController)->home();
}
