<?php

return [
    '/' =>                  'MainController@home',
    '/logout' =>            'MainController@logout',

    '/auth-code/auth-discord' =>      'AuthorizationCodeController@authDiscord',
    '/auth-code/data-discord' =>      'AuthorizationCodeController@dataDiscord',
    '/auth-code/auth-battlenet' =>    'AuthorizationCodeController@authBattlenet',
    '/auth-code/data-battlenet' =>    'AuthorizationCodeController@dataBattlenet',
    '/provider-discord/auth-code' =>  'AuthorizationCodeController@discordProvider',
    '/provider-battlenet/auth-code' => 'AuthorizationCodeController@battlenetProvider',

    '/provider-discord/password' => 'PasswordController@DiscordProvider',

    '/provider-discord/client-credential' => 'ClientCredentialController@DiscordProvider',
    '/provider-battlenet/client-credential' => 'ClientCredentialController@BattlenetProvider'
];
