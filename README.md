# Client Provider pour OAuth 2.0 

## Préambule
Dans le cadre d'un exercice, nous avons effectué le développement d'un SDK permettant de se connecter à des providers **OAuth 2.0**. Il y a une partie client et une partie serveur. Les deux parties ont pour consigne d'être orienté objet, puis, la partie cliente laisse libre le choix de 2 providers.

Nous avons choisis comme Provider client :
- Discord
- Battlenet (Blizzard)

## Installation
Commencez par copier/coller le dossier `oauth2` et le fichier `autoload.php` se situant à la racine du projet, à la racine de votre projet php. Puis, faites une importation du fichier `autoload.php` depuis de votre script php.
```php
    require __DIR__ . '/autoload.php';
```

### Changer le dossier où se trouve notre librairie
Ouvrez le fichier autoload.php et dans le tableau `$autoloadContent`, modifiez chaque valeur (exemple 'oauth2/client') en ajoutant le nouveau chemin vers le dossier oauth2.

## Utilisation de la partie cliente
### Authorization Code
```php
    use GroupArbCw\Oauth2\Client\Provider\Discord;

    # Définition du provider (Discord ou Battlenet)
    $provider = new Discord([
        'clientId' => '{your-client-id}',
        'clientSecret' => '{your-client-secret}',
        'redirectionUrl' => '{your-redirection-url}',
        'scope' => '{an-optionnal-scope}',
    ]);
    # Sélection de la méthode d'authentification à utiliser
    $provider->useAuthorizationCode();

    # Connexion au serveur d'api oauth2 de discord
    if (!isset($_GET['code'])) {
        
        $_SESSION['oauth2_state'] = $provider->getState();
        $provider->sendAuthorizeRequest();
    } else if (empty($_GET['state']) || $_SESSION['oauth2_state'] !== $_GET['state']) {
        ## Une erreur est apparu
        unset($_SESSION['oauth2_state']);
        die('Authentification error');
    } else {
        ## L'utilisateur a été authentifié et a reçu un code. Il doit l'échanger contre un token
        $token = $provider->getToken([
            'code' => $_GET['code']
        ]);
        
        ## On récupère ici les informations de l'utilisateur en exploitant le token
        $user = $provider->getResourceOwner($token);
    }
```

### Client Credential
```php
    use GroupArbCw\Oauth2\Client\Provider\Discord;

    # Définition du provider (Discord ou Battlenet)
    $provider = new Discord([
        'clientId' => '{your-client-id}',
        'clientSecret' => '{your-client-secret}',
        'scope' => '{an-optionnal-scope}',
    ]);
    # Sélection de la méthode d'authentification à utiliser
    $provider->useClientCredentials();

    ## L'utilisateur va être identifié à l'aide de son clientId et son clientSecret.
    ## On obtient ensuite son token
    $token = $provider->getToken();

    ## On récupère ici les informations de l'utilisateur en exploitant le token
    $user = $provider->getResourceOwner($token);
```
### Password
```php
    use GroupArbCw\Oauth2\Client\Provider\Discord;

    # Définition du provider (Discord ou Battlenet)
    $provider = new Discord([
        'clientId' => '{your-client-id}',
        'clientSecret' => '{your-client-secret}',
        'scope' => '{an-optionnal-scope}',
    ]);
    # Sélection de la méthode d'authentification à utiliser
    $provider->usePassword();

     ## L'utilisateur va être identifié par les identifiants suivants.
     ## Si tout se passe bien, il recevra un token, sinon le token vaudra null.
    $token = $provider->getToken([
        'username' => 'myUsername',
        'password' => '12345678'
    ]);

    ## On récupère ici les informations de l'utilisateur en exploitant le token
    $user = $provider->getResourceOwner($token);
```
## Démonstration
Il existe une démonstration que vous pouvez lancer en exécutant le fichier `index.php` se trouvant dans le dossier `/demo`.