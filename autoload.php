<?php

/**
* Autoloader : Pour le chargement dynamique des classe
*/

spl_autoload_register(function ($classFullyQualified) {
    $autoloadContent = [
        'GroupArbCw\Oauth2\Client' => 'oauth2/client',
        'GroupArbCw\Oauth2\Client\Provider' => 'oauth2/client/provider',
        'GroupArbCw\Oauth2\Server' => 'oauth2/server'
    ];

    ### Extract namespace and classe name ###
    $segmentedNamespace = explode('\\', $classFullyQualified);
    $classname = array_pop($segmentedNamespace);
    $namespace = implode('\\', $segmentedNamespace);

    if (key_exists($namespace, $autoloadContent)) {
        $classPath = $autoloadContent[$namespace] . '/' . $classname . '.php';
        include $classPath;
    }
});
