<?php

namespace GroupArbCw\Oauth2\Server;

use DateTime;

class Provider
{
    public function auth()
    {
        // Get request parameters
        [
            'response_type' => $response_type,
            'client_id'     => $client_id,
            'state'         => $state
        ] = $_GET;

        $client = $this->getClient($client_id);
        if ($client) {
            echo $client['name'];
            echo $client['uri'];
            echo "<a href='/auth-success?client_id=" . $client_id . "&state=" . $state . "'>Approuve</a>";
        } else {
            http_response_code(404);
        }
    }

    public function authSuccess()
    {
        [
            'client_id' => $client_id,
            'state'     => $state
        ] = $_GET;

        $code = uniqid();
        $expiration = new DateTime('+ 120 seconds');
        $data = $this->read_file('./data/code.data');
        $data[] = [
            'code'       => $code,
            'expiration' => $expiration,
            'client_id'  => $client_id
        ];
        $this->write_file($data, './data/code.data');

        header('Location: ' . $this->getClient($client_id)['redirect_success'] . '?code=' . $code . '&state=' . $state);
    }

    public function exchangeAuthorizationCodeToToken(array $client, string $code)
    {
        if ($code) {
            $client_code = $this->getCode($client["client_id"], $code);
            // Check code validity
            if ($client_code && $client_code['expiration'] > new DateTime()) {
                // Generate token
                $token = uniqid();
                $token_expiration = new DateTime('+ 1 hours');
                $user_id = uniqid();
                // Save into database
                $data = $this->read_file('./data/token.data');
                $data[] = [
                    'token' => $token,
                    'token_expiration' => $token_expiration,
                    'user_id' => $user_id
                ];
                $this->write_file($data, './data/token.data');
                // Send it to client server
                echo json_encode(["access_token" => $token, "expirationDate" => $token_expiration]);
            }
        }
    }

    public function exchangeClientCredentialToToken(array $client)
    {
        // Generate token
        $token = uniqid();
        $token_expiration = new DateTime('+ 1 hours');
        // Save into database
        $data = $this->read_file('./data/token_client.data');
        $data[] = [
            'client_id' => $client['client_id'],
            'token' => $token,
            'token_expiration' => $token_expiration,
        ];
        $this->write_file($data, './data/token_client.data');
        // Send it to client server
        echo json_encode(["access_token" => $token, "expirationDate" => $token_expiration]);
    }

    public function exchangePasswordToToken(string $username, string $password)
    {
        if ($username && $password) {
            if ($username == 'user' && $password == 'password') {
                // Generate token
                $token = uniqid();
                $token_expiration = new DateTime('+ 1 hours');
                $user_id = uniqid();
                // Save into database
                $data = $this->read_file('./data/token.data');
                $data[] = [
                    'token' => $token,
                    'token_expiration' => $token_expiration,
                    'user_id' => $user_id
                ];
                $this->write_file($data, './data/token.data');
                // Send it to client server
                echo json_encode(["access_token" => $token, "expirationDate" => $token_expiration]);
            }
        }
    }

    public function getApp(string $url)
    {
        $data = $this->read_file('./data/app.data');
        foreach ($data as $value) {
            if ($value['uri'] === $url) {
                return $value;
            }
        }
        return false;
    }

    public function getClient($id)
    {
        $data = $this->read_file('./data/app.data');
        foreach ($data as $value) {
            if ($value['client_id'] === $id) {
                return $value;
            }
        }
        return false;
    }

    public function getCode($client_id, string $code)
    {
        $data = $this->read_file('./data/code.data');
        foreach ($data as $value) {
            if ($value['client_id'] === $client_id && $value['code'] === $code) {
                return $value;
            }
        }
        return false;
    }

    public function register()
    {
        [
            "name" => $name,
            "uri" => $uri,
            "redirect_success" => $redirect_sucess,
            "redirect_error" => $redirect_error
        ] = $_POST;
        // Test if app already registered
        $app = $this->getApp($uri);
        if ($app) {
            echo ("Url Already registered");
        } else {
            $data = $this->read_file('./data/app.data');
            $client_id = uniqid('client_', true);
            $data[] = [
                "name" => $name,
                "uri" => $uri,
                "redirect_success" => $redirect_sucess,
                "redirect_error" => $redirect_error,
                "client_id" => $client_id,
                "client_secret" => sha1($client_id)
            ];
            $this->write_file($data, './data/app.data');

            header('Content-Type: application/json');
            echo (json_encode([
                "client_id" => $client_id,
                "client_secret" => sha1($client_id)
            ]));
        }
    }

    public function token()
    {
        // Get request parameters
        [
            'grant_type' => $grant_type,
            'client_id' => $client_id,
            'redirect_uri' => $redirect_uri,
            'client_secret' => $client_secret
        ] = $_GET;
        // Check client credentials
        $client = $this->getClient($client_id);
        if ($client && $client['client_secret'] === $client_secret) {
            switch ($grant_type) {
                case 'authorization_code':
                    ['code' => $code] = $_GET;
                    $this->exchangeAuthorizationCodeToToken($client, $code);
                    break;
                case 'client_credentials':
                    $this->exchangeClientCredentialToToken($client);
                    break;
                case 'password':
                    // Get REQUEST PARAMS for PASSWORD process
                    [
                        'username' => $username,
                        'password' => $password
                    ] = $_GET;
                    $this->exchangePasswordToToken($username, $password);
                    break;
            }
        }
    }

    public function write_file(array $data, string $filename)
    {
        $data = array_map(fn ($item) => serialize($item), $data);
        return file_put_contents($filename, implode(PHP_EOL, $data));
    }

    public function read_file(string $filename)
    {
        if (!file_exists($filename)) throw new \Exception($filename . ' not found');
        $data = file($filename);
        return array_map(fn ($item) => unserialize($item), $data);
    }
}
