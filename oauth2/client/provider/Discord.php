<?php

namespace GroupArbCw\Oauth2\Client\Provider;

use GroupArbCw\Oauth2\Client\AbstractProvider;

class Discord extends AbstractProvider
{
    # Définit les url permettant d'exploiter les serveurs oauth2 de discord
    protected $authorizeUrl = 'https://discord.com/api/oauth2/authorize';
    protected $tokenUrl = 'https://discord.com/api/oauth2/token';
    protected $credentialsUrl = 'https://discordapp.com/api/users/@me';

    public function __construct(array $data)
    {
        parent::__construct($data);

        # Affecte le scope identify par défaut
        $this->scope = $data['scope'] ?? 'identify';
    }
}
