<?php

namespace GroupArbCw\Oauth2\Client\Provider;

use GroupArbCw\Oauth2\Client\AbstractProvider;

class Battlenet extends AbstractProvider
{
    # Définit les url permettant d'exploiter les serveurs oauth2 de blizzard
    protected $authorizeUrl = 'https://{region}.battle.net/oauth/authorize';
    protected $tokenUrl = 'https://{region}.battle.net/oauth/token';
    protected $credentialsUrl = 'https://eu.battle.net/oauth/userinfo';

    # Battlenet a besoin d'une région dans l'URL
    private $region = 'eu';

    public function __construct($data)
    {
        parent::__construct($data);

        # On va considérer le region EU
        $this->region = (key_exists('region', $data) ? $data['region'] : $this->region);

        # Affecte le scope identify par 
        $this->scope = $data['scope'] ?? 'wow.profile';
    }

    # Les 2 méthodes suivantes surchargent les méthodes permettant d'obtenir les url d'authorisation et de token pour retourner dynamiquement les url comprennant une région définit également dynamiquement
    public function getAuthorizeUrl(): string
    {
        return $this->applyRegionToUrl(parent::getAuthorizeUrl());
    }

    public function getTokenUrl(): string
    {
        return $this->applyRegionToUrl(parent::getTokenUrl());
    }

    /**
     * Change le champ {region} de l'url fournit en argument avec la valeur de $this->region
     */
    private function applyRegionToUrl(string $url)
    {
        return str_replace('{region}', $this->region, $url);
    }
}
