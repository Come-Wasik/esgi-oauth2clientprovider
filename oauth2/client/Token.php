<?php

namespace GroupArbCw\Oauth2\Client;

/**
 * Permet de contenir et d'exploiter un token sous forme d'objet
 */
class Token
{
	private $access_token;
	private $expires_in;
	private $refresh_token;
	private $scope;
	private $token_type;

	public function __construct(array $data)
	{
		# Hydratation
		# Tableau de correspondance entre un attribut et son setter
		$setterList = [
			'access_token' => 'setAccessToken',
			'expires_in' => 'setExpiresIn',
			'refresh_token' => 'setRefreshToken',
			'scope' => 'setScope',
			'token_type' => 'setToken_type',
		];

		# Affectation du setter si l'attribut existe
		foreach ($data as $key => $value) {
			if (key_exists($key, $setterList)) {
				$method = $setterList[$key];
				$this->$method($value);
			}
		}
	}

	public function getAccessToken(): ?string
	{
		return $this->access_token;
	}

	public function expiresIn(): ?int
	{
		return $this->expires_in;
	}

	public function getRefreshToken(): ?string
	{
		return $this->refresh_token;
	}

	public function getScope(): ?string
	{
		return $this->scope;
	}

	public function getType(): ?string
	{
		return $this->token_type;
	}

	public function setAccessToken(string $access_token): void
	{
		$this->access_token = $access_token;
	}

	public function setExpiresIn(int $expires_in): void
	{
		$this->expires_in = $expires_in;
	}

	public function setRefreshToken(string $refresh_token): void
	{
		$this->refresh_token = $refresh_token;
	}

	public function setScope(string $scope): void
	{
		$this->scope = $scope;
	}

	public function setToken_type(string $token_type): void
	{
		$this->token_type = $token_type;
	}
}
