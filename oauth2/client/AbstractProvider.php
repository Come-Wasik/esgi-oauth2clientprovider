<?php

namespace GroupArbCw\Oauth2\Client;

use Exception;

/**
 * Contient toutes les méthodes permettant de créer un provider client par héritage
 */
abstract class AbstractProvider
{
    # Permet d'effectuer des requêtes get et post à l'aide d'une seule fonction
    use RequestMaker;

    # Par défaut, c'est la méthode d'authentification authorization_code qui est utilisée
    protected $responseType = 'code';
    protected $grantType = 'authorization_code';

    /** @var string $authorizeUrl La classe enfant doit modifier cette donnée */
    /** @var string $tokenUrl La classe enfant doit modifier cette donnée */
    /** @var string $credentialsUrl La classe enfant doit modifier cette donnée */
    protected $authorizeUrl;
    protected $tokenUrl;
    protected $credentialsUrl;

    /** @var string $tokenUrlHeader Spécifie le content-type à utiliser pour effectuer des requêtes visant à récupérer un token */
    protected $tokenUrlHeader = 'application/x-www-form-urlencoded';

    /** @var string $clientId Il s'agit du client id de l'application de l'utilisateur */
    /** @var string $clientSecret Il s'agit du client secret de l'application de l'utilisateur */
    /** @var string $redirectionUrl Il s'agit de l'url suivi après authentification */
    /** @var string $scope Il s'agit des informations à requêter après authentification */
    /** @var string $state Il s'agit d'une chaîne désigné à éviter les attaques de type csrf */
    protected $clientId;
    protected $clientSecret;
    protected $redirectionUrl;
    protected $scope;
    protected $state;

    /**
     * Permet d'instancier un provider hérité de AbstractProvider
     * 
     * La méthode prend en entrée les informations permettant d'initialiser le provider
     *
     * @param array $data Contient les informations permettant d'initialiser le provider
     **/
    public function __construct(array $data)
    {
        # Remplissage des informations du provider
        $this->clientId = $data['clientId'] ?? '';
        $this->clientSecret = $data['clientSecret'] ?? '';
        $this->redirectionUrl = $data['redirectionUrl'] ?? '';

        # Définition du state
        $this->state = hash('md5', $_COOKIE['PHPSESSID']);
        $_SESSION['state'] = $this->state;
    }

    /**
     * Revoit la valeur de la variable d'instance $state
     * 
     * @return string
     **/
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * Renvoi l'url d'authorisation (à définir dans le provider enfant)
     * 
     * @return string
     * @throws Exception L'url doit être définit et non vide
     **/
    public function getAuthorizeUrl(): string
    {
        if (!isset($this->authorizeUrl)) {
            throw new Exception('The authorization url is not defined by the administrator', 500);
        }
        if (empty($this->authorizeUrl)) {
            throw new Exception('The authorization url is empty by the administrator', 500);
        }

        return $this->authorizeUrl;
    }

    /**
     * Renvoi l'url de token (à définir dans le provider enfant)
     * 
     * @return string
     * @throws Exception L'url doit être définit et non vide
     **/
    public function getTokenUrl(): string
    {
        if (!isset($this->tokenUrl)) {
            throw new Exception('The token url is not defined by the administrator', 500);
        }
        if (empty($this->tokenUrl)) {
            throw new Exception('The token url is empty by the administrator', 500);
        }

        return $this->tokenUrl;
    }

    /**
     * Revoit l'url pour obtenir ses "credentials" ou information utilisateur
     * 
     * @return string
     **/
    public function getCredentialUrl()
    {
        return $this->credentialsUrl;
    }

    /**
     * Effectue la requête qui va nous rediriger et nous renvoyer sur l'url de redirection.
     * Il faudra alors récupérer le code afin de récupérer le token
     * 
     **/
    public function sendAuthorizeRequest()
    {
        $params = [
            'response_type' => $this->responseType,
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectionUrl,
            'scope' => $this->scope,
            'state' => $this->state
        ];
        $autorizeFullUrl = $this->getAuthorizeUrl() . '?' . http_build_query($params);

        header('Location: ' . $autorizeFullUrl);
        die();
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param array $data Contient des données à utiliser pour récupérer le token (code, user:password ...)
     * @return Token ou null
     * @throws Exception Grant type doit être connu [authorization_code, client_credentials ou password]
     **/
    public function getToken(array $data = []): ?Token
    {
        switch ($this->grantType) {
            case 'authorization_code':
                $response = $this->getTokenByAuthCode($data['code']);
                break;

            case 'client_credentials':
                $response = $this->getTokenByClientCredential();
                break;

            case 'password':
                $response = $this->getTokenByPassword($data['username'], $data['password']);
                break;
            default:
                throw new Exception('Grand type unknown', 500);
        }

        if ($response === null) {
            return null;
        }

        $token = new Token(json_decode($response, true));
        return $token;
    }

    /**
     * Utilise la méthode d'authentification authorization_code pour obtenir un token
     *
     * @param string $code Est le code à échanger contre un token
     * @return string (json)
     **/
    public function getTokenByAuthCode(string $code)
    {
        $params = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'grant_type' => $this->grantType,
            'code' => $code,
            'redirect_uri' => $this->redirectionUrl,
            'scope' => $this->scope
        ];

        $response = $this->fetch($this->getTokenUrl(), 'POST', $params, ['Content-Type: ' . $this->tokenUrlHeader]);
        return $response;
    }

    /**
     * Utilise la méthode d'authentification client_credentials pour obtenir un token
     *
     * @return string (json)
     **/
    public function getTokenByClientCredential()
    {
        $params = [
            'grant_type' => $this->grantType,
            'scope' => $this->scope
        ];

        $response = $this->fetch($this->getTokenUrl(), 'POST', $params, [
            'Content-Type: ' . $this->tokenUrlHeader,
            'Authorization: Basic ' . base64_encode("$this->clientId:$this->clientSecret")
        ]);
        return $response;
    }

    /**
     * Utilise la méthode d'authentification password pour obtenir un token
     *
     * @param string $username C'est le nom d'utilisateur à utiliser
     * @param string $password C'est le mot de passe utilisateur à utiliser
     * @return string (json)
     **/
    public function getTokenByPassword(string $username, string $password)
    {
        $params = [
            'grant_type' => $this->grantType,
            // 'redirect_uri' => $this->redirectionUrl,
            'scope' => $this->scope,
            'username' => $username,
            'password' => $password
        ];

        $response = $this->fetch($this->getTokenUrl(), 'POST', $params, [
            'Content-Type: ' . $this->tokenUrlHeader,
            'Authorization: Basic ' . base64_encode("$username:$password")
        ]);
        if ($response === null) {
            throw new Exception('Le token ne peut être délivré', 500);
        }
        return $response;
    }

    /**
     * Retourne les informations de l'utilisateur obtenus en utilisant un token
     *
     * @param Token $token C'est le token à échanger contre les informations de l'utilisateur
     * @return array
     **/
    public function getResourceOwner(Token $token)
    {
        $response = $this->fetch($this->getCredentialUrl(), 'GET', [], ['authorization: ' . $token->getType() . ' ' . $token->getAccessToken()]);
        return json_decode($response, true);
    }

    /**
     *  Affectation d'un mode d'authentification au provider
     *  ----------------------------------------------------
     */

    /**
     * Permet l'utilisation du authorization code grant
     **/
    public function useAuthorizationCode()
    {
        $this->responseType = 'code';
        $this->grantType = 'authorization_code';
    }

    /**
     * Permet l'utilisation du client credentials grant
     **/
    public function useClientCredentials()
    {
        $this->grantType = 'client_credentials';
    }

    /**
     * Permet l'utilisation du password grant
     **/
    public function usePassword()
    {
        $this->grantType = 'password';
    }
}
