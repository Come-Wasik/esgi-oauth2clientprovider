<?php

namespace GroupArbCw\Oauth2\Client;

/**
 * Permet d'effectuer des requêtes GET ou POST qui renverra un résultat
 */
trait RequestMaker
{
    /**
     * Crée un flux pour générer une requête (GET/POST) à exécuter puis renvoi son résultat
     * 
     * @param string $url
     * @param string $method
     * @param string $args
     * @param array $headers
     * @return string 
     */
    public function fetch($url, $method = 'GET', $args = [], array $headers = [])
    {
        $context = stream_context_create([
            'http' => [
                'header' => (!empty($headers) ? rtrim(implode("\r\n", $headers), "\r\n") . "\r\n" : ''),
                'method' => $method,
                'content' => (!empty($args) ? http_build_query($args) : '')
            ]
        ]);
        $response = file_get_contents($url, false, $context);
        return $response;
    }
}
